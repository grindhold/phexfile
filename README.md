libphexfile
========

A storage format for multidimensional data

Description
-----------

Phexfile is a storage format for multidimensional data. libphexfile is an implementation of
Phexfile that guarantees linear access time no matter how many dimensions are involved. It
focuses on providing two-dimensional cuts through n-dimensional spaces. Its primary use is
storing data created by quantum-chemical simulations that analyze changes in the potential
energy surface of a molecule in respect to geometrical manipulations according to several
coordinates.

Dependencies
------------

  * glib-2.0
  * gobject-2.0
  * gio-2.0
  * json-glib-1.0

Building
--------

[Meson > 0.40.0](http://mesonbuild.com) is the buildsystem for libphexfile. The build dependencies
are the following:

  * valac > 0.32 - Vala compiler
  * valadoc - Vala documentation tool
  * g-ir-compiler - GObject introspection compiler (package `gobject-introspection` in ubuntu)
  * json-glib-dev - Dev headers for json-glib (package `libjson-glib-dev` in ubuntu)
  * libgirepository1.0-dev - Dev headers for gobject introspection

Ubuntu: `sudo apt install valac valadoc gobject-introspection libjson-glib-dev libgirepository1.0-dev`

Clone and build the library as follows:

```bash
git clone https://notabug.org/grindhold/libphexfile
mkdir libphexfile/build
cd libphexfile/build
meson
ninja
```

Add `--prefix=/usr` to install systemwide on ubuntu. Add `--buildtype=release` for a release build.

If you desire to install the library, execute as root:

```bash
ninja install
```

License
-------

Libphexfile is published under the GNU Lesser General Public License v3 or later.
