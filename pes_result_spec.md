{
    "title": "My Simulation",
    "description" : "some longer text about my simulation. that answers which purpose it serves", 
    "begin" : "2018-01-01T12:00:00+0000",  /* Date at which the calculation started */
    "end" : "2018-01-01T12:00:00+0000",  /* Date at which the calculation started */

    // Followed by a list of coordinates that are being investigated in this simulation
    // Contains anything that is needed to reconstruct the step-width into a proper scale
    "coordinates":[
        {
            "name":"Dihedral C4 C23 C24 C6",
            "steps": 10,
            "step_width": 1,
            "from": 0,
            "unit": "radians",
            "fac": "1.57/10"
        },
        {
            "name":"Dihedral C23 C24 C6 C8",
            "steps": 10,
            "step_width": 1,
            "from": 0,
            "unit": "radians",
            "fac": "3.14/10"
        }
    ],

    // list of excitations
    "excitations": [
        "S0",
        "S1",
        "T1",
    ],

    // a list of used methods to calculate the energy. this is bascally a free string
    // that should include the basis set used, the method used and some crucial parameters
    // if the person executing the simulation feels that it is important to note.
    "methods": [
        "cc-eom/sto-3g",
        "cc-eom/aug-cc-pvdz",
        "scf/sto-3g",
        "scf/aug-cc-pvdz"
    ],

    // the actual data of the format.
    // It is organized in the following way:
    // the first n entries of the list are the respective steps of the reaction coordinates
    // that the contained energies belong to in the order that the reaction coordinates occur
    // in the "coordinates"-field.
    // They are followed by a n*m matrix representing the energies in different excitation
    // states versus different methods that have been used in the simulation. The energy states
    // represent the matrix columns whereas the methods represent the rows.
    // The last element is a Geometry of that particular state in XYZ-format.
    // The writing software is required to increment the coordinate steps most frequently
    // at the right handside. And least frequently at the left side. This counting up must
    // be represented in the order of the list elements of "energies".

    // TODO: rephrase the last two sentences

    "energies":[
        [
            0,0,[
               [591.1234, 591.0000, 571.0000],
               [591.1234, 595.0000, 581.0000],
               [591.1234, 598.0000, 595.0000],
               [591.1234, 590.0000, 591.0000]
            ], "<GEOMETRY REPRESENTATION IN XYZ FORMAT>"
        ],[
            0,1,[
               [591.1234, 591.0000, 571.0000],
               [591.1234, 595.0000, 581.0000],
               [591.1234, 598.0000, 595.0000],
               [591.1234, 590.0000, 591.0000]
            ], "<GEOMETRY REPRESENTATION IN XYZ FORMAT>"
        ] <…>, [
            9,9,[
               [591.1234, 591.0000, 571.0000],
               [591.1234, 595.0000, 581.0000],
               [591.1234, 598.0000, 595.0000],
               [591.1234, 590.0000, 591.0000]
            ], "<GEOMETRY REPRESENTATION IN XYZ FORMAT>"],
        ]
    ]
}
