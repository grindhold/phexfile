# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of libphexfile.
#
# libphexfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# libphexfile is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with libphexfile.
# If not, see http://www.gnu.org/licenses/.

obj = """[
            0,0,0,[
               [1.0, 2.0, 3.0],
               [4.0, 5.0, 6.0],
               [7.0, 8.0, 9.0],
               [10.0, 11.0, 12.0]
            ], "3\\nfoo\\nH  0.0  1.78 0.0\\nO  0.0  0.0  0.0\\nH  0.0  -1.78  0.0"
        ]
"""

import json

m = json.loads(obj)
energies = []
for i in range(0,10):
    for j in range(0,10):
        for k in range(0,10):
            energies.append(m.copy())
            energies[-1][0] = i
            energies[-1][1] = j
            energies[-1][2] = k 
            energies[-1][3] = []
            x = 1.0
            for s in range(0,4):
                energies[-1][3].append([])
                for e in range(0,3):
                    energies[-1][3][-1].append(x + 0.1*i + 0.01*j + 0.001*k)
                    x += 1
            energies[-1][4] = "3\nfoo\nH  0.0  {} 0.0\nO  0.0  0.0  0.0\nH  0.0  {}  0.0".format(0.1*i + 0.01*j, 0.1*i + 0.01*j + 0.001*k)

print (json.dumps(energies, indent=4))
