#!/usr/bin/python3
# Copyright 2018-2019 Daniel 'grindhold' Brendle
#
# This file is part of libphexfile.
#
# libphexfile is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# libphexfile is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with libphexfile.
# If not, see http://www.gnu.org/licenses/.

import math
import json

"""
This is the correct axis-order on how to design a simulation
"""


result = {
    "title": "Different Mathematical functions",
    "description": \
"""This simulation researches the potential energy surface
of azobenzene along two reaction coordinates: The dihedral
along the double bond of the azo-group and the dihedral
of the phenol connection to the azo group. The ground state
and the first excitation state are being examined.

This calculates all excitation energies in one run! Do compare
against the first run in which the excitation energies have been
calculated separately.
""",
    "coordinates": [
        {
            "name": "Sine Axis",
            "steps": 100,
            "step_width": 1,
            "from": 0,
            "unit": "°",
            "fac": 1
        },
        {
            "name": "Cosine Axis",
            "steps": 100,
            "step_width": 1,
            "from": 0,
            "unit" : "°",
            "fac": 1
        },
        {
            "name": "Linear Axis",
            "steps": 100,
            "step_width": 1,
            "from": 0,
            "unit" : "°",
            "fac": 1
        }
    ],

    "excitations": ["S0"],
    "methods": ["sto-3g/eom-cc2"],
    "energies": []
}

def genval():
    return math.sin(i)

for i in range(result["coordinates"][0]["steps"]):
    for j in range(result["coordinates"][1]["steps"]):
        for k in range(result["coordinates"][2]["steps"]):
            result["energies"].append( [i,j,k,[[ math.sin(i*math.pi / 15)*2 + math.cos(j*math.pi/15)*2 + k ]], "2\nLOL\n H 0.0 0.0 0.0\n H 0.0 1.0 0.0"])

print (json.dumps(result))
